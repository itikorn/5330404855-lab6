<?php
// transform.php
header('Content-type: text/html; charset=utf-8');

$xsl_filename = "summaryBlogs.xsl";
$xml_filename = "summaryBlogs.xml";
$doc = new DOMDocument();
$xsl = new XSLTProcessor();
$doc->load($xsl_filename);
$xsl->importStyleSheet($doc);
$doc->load($xml_filename);
echo $xsl->transformToXML($doc);
?>